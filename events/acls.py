import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
from .models import Location


def get_photo(city, state):
    request_url = f"https://api.pexels.com/v1/search?query={city}&{state}"
    headers = {"authorization": PEXELS_API_KEY}
    response = requests.get(request_url, headers=headers)
    picture = json.loads(response.content)
    picture_url = {"picture_url": picture["photos"][0]["url"]}
    return picture_url


def get_weather_data(city, state):
    pass
    # headers = {"authorization": OPEN_WEATHER_API_KEY}
    # geocode_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&limit={1}&appid={headers}"
    # response = requests.get(geocode_url, headers=headers)
    # geocode = json.loads(response.content)
    # lat = geocode[coord.lat]
    # lon = geocode[coord.lon]
    # weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={headers}"
    # weather_response = requests.get(weather_url, headers=headers)
    # weather_data = json.loads(weather_response.content)
    # weather_return = {"weather_description": weather_data["weather"][0]["description"],
    #                "weather_temp": weather_data["main"]["temp"]}
    # return weather_return
